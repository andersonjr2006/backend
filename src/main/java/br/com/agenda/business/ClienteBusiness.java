package br.com.agenda.business;

import java.util.List;

import javax.persistence.EntityManager;

import br.com.agenda.DAO.ClienteDAO;
import br.com.agenda.model.entity.Cliente;

public class ClienteBusiness {

	EntityManager em;
	private ClienteDAO clienteDAO;

	public ClienteBusiness(EntityManager em) {
		this.em = em;
		this.clienteDAO = new ClienteDAO(em);
	}

	public EntityManager getEm() {
		return this.em;
	}

	public void setEm(EntityManager em) {
		this.em = em;
	}

	public Cliente carregar(Long idCliente) {
		return this.clienteDAO.carregar(idCliente);
	}

	public Cliente salvar(Cliente cliente) {
		return this.clienteDAO.salvar(cliente);
	}

	public Cliente buscaPorId(Long id) {
		return this.clienteDAO.buscarPorId(id);
	}

	public boolean excluir(long id) {
		return this.clienteDAO.excluir(id);
	}
	public List<Cliente> listar() {
		return this.clienteDAO.listar();
	}
}
