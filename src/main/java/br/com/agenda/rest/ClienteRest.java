package br.com.agenda.rest;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import br.com.agenda.business.ClienteBusiness;
import br.com.agenda.commons.HibernateUtil;
import br.com.agenda.model.entity.Cliente;

@Path("/cliente")
public class ClienteRest {
	private EntityManager em = HibernateUtil.getEntityManagerFactory().createEntityManager();
	private ClienteBusiness clienteBusiness;

	public ClienteRest() {
		this.clienteBusiness = new ClienteBusiness(this.em);
	}
/*	@Path("/teste")
	@GET
    @Produces(MediaType.TEXT_HTML)
    public String simpleMessage(@QueryParam("nome") String nome) {

        return "<p>This is a simple REST</p>" + " " + nome;

    }*/
	
	@GET
	@Path("/get")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getClienteById(@QueryParam("id") Long id) {
		Cliente cliente = new Cliente();
		cliente = this.clienteBusiness.buscaPorId(id);
		this.closeSessions();
		return Response.ok()
	               .entity(cliente)
	               .header("Access-Control-Allow-Origin", "*")
	               .build();
	}
	@POST
	@Path("/listar")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response lista() {
		List<Cliente> lista = new ArrayList<Cliente>();
		lista = this.clienteBusiness.listar();
		this.closeSessions();
		return Response.ok()
	               .entity(lista)
	               .header("Access-Control-Allow-Origin", "*")
	               .build();
	}

	@POST
	@Path("/salvar")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response create(Cliente cliente) {
		cliente = this.clienteBusiness.salvar(cliente);
		this.closeSessions();
		return Response.ok()
	               .entity(cliente)
	               .header("Access-Control-Allow-Origin", "*")
	               .build();
	}

	@GET
	@Consumes({ "application/json" })
	@Path("/excluir")
	public Response remove(@QueryParam("id") Long id) {
		this.clienteBusiness.excluir(id);
		this.closeSessions();
		return Response.ok()
	               .header("Access-Control-Allow-Origin", "*")
	               .build();
	}
	
	public void closeSessions() {
		this.clienteBusiness.getEm().close();
	}
}