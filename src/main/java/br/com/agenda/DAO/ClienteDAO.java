package br.com.agenda.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.agenda.model.entity.Cliente;

public class ClienteDAO {
	private EntityManager em;
	
	public ClienteDAO(EntityManager em) {
		this.em = em;
	}

	public Cliente salvar(Cliente cliente) {
		try {
			cliente = (Cliente) this.saveOrUpdate(cliente);
		} catch (Exception var3) {
			var3.printStackTrace();
		}

		return cliente;
	}

	public boolean excluir(long id) {
		try {
			Cliente cliente = this.carregar(id);
			this.delete(cliente);
			return true;
		} catch (Exception var4) {
			return false;
		}
	}

	public Cliente buscarPorId(Long id) {
		String Sql = "select u from Cliente u where u.id =:id ";
		Query consulta = this.em.createQuery(Sql);
		consulta.setParameter("id", id);
		List<Cliente> cliente = consulta.getResultList();
		if (cliente.size() == 0) {
			cliente.add(new Cliente());
		}

		return (Cliente) cliente.get(0);
	}
	
	public Cliente carregar(Long id) {
		String sql = "select u from Cliente u where u.id = :id";
		Query consulta = this.em.createQuery(sql);
		consulta.setParameter("id", id);
		List<Cliente> cliente = (List<Cliente>) consulta.getResultList();
		if(cliente.size() == 0) {
			cliente.add(new Cliente());
		}
		return cliente.get(0);
	}

	public <T> T delete(T t) throws Exception {
		try {
			this.em.getTransaction().begin();
			this.em.remove(t);
			this.em.getTransaction().commit();
		} catch (Exception var6) {
			throw var6;
		} finally {
			this.em.clear();
		}

		return t;
	}

	public <T> T saveOrUpdate(T t) throws Exception {
		try {
			this.em.getTransaction().begin();
			if (((Cliente) t).getId() == null) {
				this.em.persist(t);
			} else {
				this.em.merge(t);
			}

			this.em.getTransaction().commit();
		} catch (Exception var6) {
			throw var6;
		} finally {
			this.em.clear();
		}

		return t;
	}
	public List<Cliente> listar() {
		String sql = "select u from Cliente u";
		Query consulta = this.em.createQuery(sql);
		List<Cliente> cliente = (List<Cliente>) consulta.getResultList();
		if(cliente.size() == 0) {
			cliente.add(new Cliente());
		}
		return cliente;
	}

}
