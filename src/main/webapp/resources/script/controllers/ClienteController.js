var ClienteCtrl = angular.module('clienteCtrl', []);
app.controller('clienteCtrl', [
		'$scope',
		'$location',
		'$http',
		'$filter',
		
		'$rootScope',
		function($scope, $location, $http, $filter, $rootScope) {			
			$scope.cliente = {};
			$scope.clientes = [];
			$scope.index = true;
			$scope.edit = 'novo';

			$scope.clienteHtml = function() {
				$location.path('/cliente');
			}
			$scope.caminho = CONTEXTO;
			$scope.caminho2 = URL;
			$scope.alterar = function(cliente) {
				$scope.cliente = cliente;
				$scope.index = false;
				$scope.edit = 'update';
				};

			$scope.novo = function() {
				$scope.cliente = {};
				$scope.index = false;
				$scope.edit = 'create';
			}
			

			$scope.salvar = function(cliente) {
				
				if($scope.edit == 'create') {
					$scope.create(cliente);
				} else {
					$scope.update(cliente);
				}
			}

			$scope.create = function(cliente) {
				
				setTimeout(function() {
					$rootScope.isVisible.loading = true;
					setTimeout(function() {
						$http({
							url : URL + "/cliente/salvar",
							method : "POST",
							contentType : "application/json",
							data : cliente
						}).success(function(data) {
							$rootScope.isVisible.loading = false;
							$scope.cliente = data;
							$scope.index = true;
							$scope.edit = 'update';
							$scope.listar();
						}).error(
							function(erro) {
								$rootScope.isVisible.loading = false;
								alert("ERRO no envio dos dados ! "
										+ erro == undefined ? ""
										: erro.error);
							})
					}, 100);

				})
			};

			$scope.update = function(cliente) {
				setTimeout(function() {
					$rootScope.isVisible.loading = true;
					setTimeout(function() {
						$http({
							url : URL + "/cliente/salvar",
							method : "POST",
							contentType : "application/json",
							data : cliente
						}).success(function(data) {
							$rootScope.isVisible.loading = false;
							$scope.cliente = data;
							$scope.index = true;
							$scope.edit = 'update';
							$scope.listar();
						}).error(
							function(erro) {
								cliente.when = $scope.data;
								$rootScope.isVisible.loading = false;
								alert("ERRO no envio dos dados ! "
										+ erro == undefined ? ""
										: erro.error);
							})
					}, 100);

				})
			};

			$scope.buscarPorId = function(id) {
				setTimeout(function() {
					$rootScope.isVisible.loading = true;
					setTimeout(function() {
						$http({
							url : URL + "/cliente/get?id=" + id,
							method : "GET"
						}).success(function(data) {
							$rootScope.isVisible.loading = false;
							$scope.cliente = data;
							$scope.alterar($scope.cliente);
							$scope.update($scope.cliente);
 
						}).error(
							function(erro) {
								$rootScope.isVisible.loading = false;
								alert("ERRO no envio dos dados ! "
										+ erro == undefined ? ""
										: erro.error);
							})
					}, 100);

				})
			};
			$scope.excluir = function(id) {
				if (!confirm('Deseja Excluir o Cliente ?')) {
					return;
				}
				
				$rootScope.isVisible.loading = true;
				setTimeout(function() {
					setTimeout(function() {
						$http({
							url : URL + "/cliente/excluir?id="+id,
							method : "GET"
						}).success(function(data) {
							$rootScope.isVisible.loading = false;
							$scope.listar();
							})
						.error(
								function(erro) {
									$rootScope.isVisible.loading = false;
									alert("ERRO no envio dos dados ! "
											+ erro == undefined ? ""
											: erro);
								})
					}, 100);
				},)}

			$scope.listar = function() {
				setTimeout(function() {
					$rootScope.isVisible.loading = true;
					setTimeout(function() {
						$http({
							url : URL + "/cliente/listar",
							method : "POST"
						}).success(function(data) {
							$rootScope.isVisible.loading = false;
							$scope.clientes = data; 
						}).error(
							function(erro) {
								$rootScope.isVisible.loading = false;
								alert("ERRO no envio dos dados ! "
										+ erro == undefined ? ""
										: erro.error);
							})
					}, 100);

				},);
			};

		
			$scope.cancelar = function() {
				$scope.listar();
				$scope.index = true;
			};

			$scope.listar();

		} ]
		);